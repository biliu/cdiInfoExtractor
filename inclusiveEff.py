import os,sys
from ROOT import *

pt_low = 20
pt_high = 300

def integral(hist, pt_low, pt_high):
  value = hist.Integral(hist.GetXaxis().FindBin(pt_low), hist.GetXaxis().FindBin(pt_high), hist.GetYaxis().FindBin(0), hist.GetYaxis().FindBin(2.5))
  return value

inputFile = TFile("merged_410470.root")
hists = {}

denHist_b = inputFile.Get("ftag/JetFtagEffPlots/B_total_AntiKt4EMPFlowJets_BTagging201903")
denHist_c = inputFile.Get("ftag/JetFtagEffPlots/C_total_AntiKt4EMPFlowJets_BTagging201903")
denHist_l = inputFile.Get("ftag/JetFtagEffPlots/L_total_AntiKt4EMPFlowJets_BTagging201903")

numHist_b_60WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_B_pass")
numHist_c_60WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_C_pass")
numHist_l_60WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_60_AntiKt4EMPFlowJets_BTagging201903_L_pass")

hists["60"] = {}
hists["60"]["B"] = numHist_b_60WP 
hists["60"]["C"] = numHist_c_60WP 
hists["60"]["L"] = numHist_l_60WP 

numHist_b_70WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_70_AntiKt4EMPFlowJets_BTagging201903_B_pass")
numHist_c_70WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_70_AntiKt4EMPFlowJets_BTagging201903_C_pass")
numHist_l_70WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_70_AntiKt4EMPFlowJets_BTagging201903_L_pass")

hists["70"] = {}
hists["70"]["B"] = numHist_b_70WP 
hists["70"]["C"] = numHist_c_70WP 
hists["70"]["L"] = numHist_l_70WP 

numHist_b_77WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_77_AntiKt4EMPFlowJets_BTagging201903_B_pass")
numHist_c_77WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_77_AntiKt4EMPFlowJets_BTagging201903_C_pass")
numHist_l_77WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_77_AntiKt4EMPFlowJets_BTagging201903_L_pass")

hists["77"] = {}
hists["77"]["B"] = numHist_b_77WP 
hists["77"]["C"] = numHist_c_77WP 
hists["77"]["L"] = numHist_l_77WP 

numHist_b_85WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_85_AntiKt4EMPFlowJets_BTagging201903_B_pass")
numHist_c_85WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_85_AntiKt4EMPFlowJets_BTagging201903_C_pass")
numHist_l_85WP = inputFile.Get("ftag/JetFtagEffPlots/DL1r_FixedCutBEff_85_AntiKt4EMPFlowJets_BTagging201903_L_pass")

hists["85"] = {}
hists["85"]["B"] = numHist_b_85WP 
hists["85"]["C"] = numHist_c_85WP 
hists["85"]["L"] = numHist_l_85WP 

wps = ["60", "70", "77", "85"]

for wp in wps:

  print (wp + " WP")

  print ("B: ")
  eff = integral(hists[wp]["B"], pt_low, pt_high)/integral(denHist_b, pt_low, pt_high)
  print (eff)

  print ("C: ")
  eff = integral(hists[wp]["C"], pt_low, pt_high)/integral(denHist_c, pt_low, pt_high)
  print (eff)

  print ("L: ")
  eff = integral(hists[wp]["L"], pt_low, pt_high)/integral(denHist_l, pt_low, pt_high)
  print (eff)
