std::vector<float> getbtaginfo(std::string cdiFile, std::string tagger ,std::string workingpoint ,std::string jetcol, std::string sfObject, float pt_low, float pt_high) {

  gErrorIgnoreLevel = kWarning;
  std::vector<float> effs;

  xAOD::TStore store;

  std::vector<int> flavors = {0,4,5};
  std::map<int, std::string> flavor_map = {{0, "Light"}, {4, "C"}, {5, "B"}}; 

  for (uint i = 0; i < flavors.size(); i++) {
    BTaggingEfficiencyTool* tool= new BTaggingEfficiencyTool("BTagTest");

    if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorFileName", cdiFile) ){ std::cout << "error initializing tool " << std::endl; return effs;  }

    if(StatusCode::SUCCESS != tool->setProperty("TaggerName",          tagger)          ){ std::cout << "error initializing tool " << std::endl; return effs;  }
    if(StatusCode::SUCCESS != tool->setProperty("OperatingPoint",      workingpoint)        ){ std::cout << "error initializing tool " << std::endl; return effs;  }
    if(StatusCode::SUCCESS != tool->setProperty("JetAuthor",           jetcol) ){ std::cout << "error initializing tool " << std::endl; return effs;  }
    //if(StatusCode::SUCCESS != tool->setProperty("VerboseCDITool",      false) ){ std::cout << "error initializing tool " << std::endl; return effs;  }
  
    std::string flavor_label = flavor_map.at(flavors.at(i)); 
 
    std::string toolName = "Efficiency" + flavor_label + "Calibrations";

    if(StatusCode::SUCCESS != tool->setProperty(toolName, sfObject) ){ std::cout << "error initializing tool " << std::endl; return effs;  }
 
    tool->setProperty("OutputLevel", MSG::WARNING).ignore();
    StatusCode code = tool->initialize();

    if (code != StatusCode::SUCCESS ) {
      std::cout << "Initialization of tool " << tool->name() << " failed! " << std::endl;
      return effs;
    }
    else {
      std::cout << "Initialization of tool " << tool->name() << " finished." << std::endl;
    }

    Analysis::CalibrationDataVariables var;
    Analysis::Uncertainty uncertainty = Analysis::Total;
 
    var.jetAuthor = jetcol;
    var.jetPt = pt_low*1000;   
    var.jetEta = 1.0;   
  
    float eff;

    tool->getMCEfficiency(flavors.at(i), var, eff);

    effs.push_back(eff);    

    std::cout<<flavor_label<<std::endl;
    std::cout<<eff<<std::endl;
    
    var.jetPt = pt_high*1000;   
    var.jetEta = 1.0;   

    tool->getMCEfficiency(flavors.at(i), var, eff);
    std::cout<<flavor_label<<std::endl;
    std::cout<<eff<<std::endl;

    effs.push_back(eff);    
  
    delete tool;
    
  }

  return effs;
}
