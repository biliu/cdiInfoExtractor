from ROOT import *
import sys,os

xAOD.Init().ignore()

gROOT.ProcessLine('.L btagtools.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)
gStyle.SetPalette(kBird)

taggers = ["DL1r"]
jet_cols = ["AntiKt4EMPFlowJets", "AntiKtVR30Rmax4Rmin02TrackJets"]
WPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]

pt_low = 20
pt_high = 300

mc_ids = ["410470", "410250", "410558", "410464", "411233", "421152", "600666", "700122"]

cdi_path = "/afs/cern.ch/user/z/zfeng/public/CDI/cdi-rel22-fix/2021-22-13TeV-MC21-CDI_smooth.root"
cdi = TFile(cdi_path)

#########################################
#    WPs definitions and Efficiencies
#########################################

for tagger in taggers:
  for jet_col in jet_cols:
    for WP in WPs:
       for mc_id in mc_ids:
         headline ="Tagger: " + tagger + ", Jet Col: " + jet_col + ", WP: " + WP
         print (headline)
         value = cdi.Get(tagger + "/" + jet_col + "/" + WP + "/cutvalue") 
         print ("Cut-value: " + str(round(value.Max(), 5)))
         print ("Efficiencies are: ")  
         effs = getbtaginfo(cdi_path, tagger, WP, jet_col, mc_id, pt_low, pt_high)

         nick_name = "PFlow"
         if "EM" not in jet_col:
           nick_name = "VR"     
         
         file_string = tagger + "_" + nick_name + "_" + WP + "_" + mc_id 
         file_name = file_string + ".tex"
 
         os.system("touch " + file_name)
         fout = open(file_name, "r+w")
         fout.write ("\\documentclass[12pt]{article}\n")
         fout.write ("\\usepackage[paperwidth=4.5in,paperheight=5in]{geometry}\n")
         fout.write ("\\usepackage{multirow}\n")
         fout.write ("\\usepackage{caption}\n")
         fout.write ("\\begin{document}\n")
         fout.write ("\\pagestyle{empty}\n")
         fout.write("\\begin{table}[ht]\n")
         fout.write("\\begin{center}\n")
         fout.write("\\captionof{table}{" + nick_name + ", " + tagger + ", " + WP.split("_")[1] + "WP, " + mc_id + "}\n")
         fout.write("\\begin{tabular}{|c|r|}\n")
         fout.write("\\hline\n")
         str_tmp = "Cut-value & " + str(round(value.Max(), 5)) + " \\\\\n"
         fout.write(str_tmp)
         fout.write("\\hline\n")
         str_tmp = "Flavor & Efficiency \\\\\n"
         fout.write(str_tmp)
         fout.write("\\hline\n")
         str_tmp = "B & " + str(pt_low) + " GeV: " + str(round(effs[4], 4)) + " \\\\\n"
         fout.write(str_tmp)
         str_tmp = "  & " + str(pt_high) + " GeV: " + str(round(effs[5], 4)) + " \\\\\n"
         fout.write(str_tmp)
         fout.write("\\hline\n")
         str_tmp = "C & " + str(pt_low) + " GeV: " + str(round(effs[2], 4)) + " \\\\\n"
         fout.write(str_tmp)
         str_tmp = "  & " + str(pt_high) + " GeV: " + str(round(effs[3], 4)) + " \\\\\n"
         fout.write(str_tmp)
         fout.write("\\hline\n")
         str_tmp = "L & " + str(pt_low) + " GeV: " + str(round(effs[0], 4)) + " \\\\\n"
         fout.write(str_tmp)
         str_tmp = "  & " + str(pt_high) + " GeV: " + str(round(effs[1], 4)) + " \\\\\n"
         fout.write(str_tmp)
         fout.write("\\hline\n")
         fout.write("\\end{tabular}\n")
         fout.write("\\end{center}\n")
         fout.write("\\end{table}\n")
         fout.write("\\end{document}\n")
         fout.close()
     
         os.system("pdflatex " + file_name) 
         os.system("convert -density 300 " + file_string + ".pdf -quality 90 " + file_string + ".png")


